import sys
from transforms import *
from images import read_img, write_img

def main():
    # LEE ARGUMENTOS
    argumentos = sys.argv[1:]

    # NOMBRE DE IMAGEN
    imagen_entrada = argumentos[0]

    imagen_en_pixels = read_img(imagen_entrada)

    # REVISA Y HACE TODAS LAS FUNCIONES
    i = 1
    while i < len(argumentos):
        funcion = argumentos[i]

        if funcion == "mirror":
            imagen_en_pixels = mirror(imagen_en_pixels)

        elif funcion == "grayscale":
            imagen_en_pixels = grayscale(imagen_en_pixels)

        elif funcion == "rotate_right":
            imagen_en_pixels = rotate_right(imagen_en_pixels)

        elif funcion == "blur":
            imagen_en_pixels = blur(imagen_en_pixels)

        elif funcion == "rotate_colors":
            incremento = int(argumentos[i + 1])
            imagen_en_pixels = rotate_colors(imagen_en_pixels, incremento)
            i += 1  # EVITA DUPLICAR EL ARGUMENTO

        elif funcion == "change_colors":
            if i + 6 >= len(argumentos):
                print("Número insuficiente de argumentos para change_colors.")
                sys.exit(1)

            prim_parametros = argumentos[i + 1:i + 4]
            to_change = []
            for t in prim_parametros:
                entero_conv = int(t)
                to_change.append(entero_conv)
            to_change = tuple(to_change)

            sec_parametros = argumentos[i + 4:i + 7]
            to_change_to = []
            for tt in sec_parametros:
                entero_conv = int(tt)
                to_change_to.append(entero_conv)
            to_change_to = tuple(to_change_to)

            imagen_en_pixels = change_colors(imagen_en_pixels, to_change, to_change_to)
            i += 6  # AVANZA 6 POS PARA EIVTAR DUPLICAR ARG

        elif funcion == "shift":
            if i + 2 >= len(argumentos):
                print("Número insuficiente de argumentos para shift.")
                sys.exit(1)

            horizontal = int(argumentos[i + 1])
            vertical = int(argumentos[i + 2])
            imagen_en_pixels = shift(imagen_en_pixels, horizontal, vertical)
            i += 2  # AVANZA 2 POS PARA EIVTAR DUPLICAR ARG

        elif funcion == "crop":
            if i + 4 >= len(argumentos):
                print("Número insuficiente de argumentos para crop.")
                sys.exit(1)

            x = int(argumentos[i + 1])
            y = int(argumentos[i + 2])
            width = int(argumentos[i + 3])
            height = int(argumentos[i + 4])
            imagen_en_pixels = crop(imagen_en_pixels, x, y, width, height)
            i += 4  # AVANZA 4 POS PARA EIVTAR DUPLICAR ARG

        elif funcion == "filter":
            if i + 3 >= len(argumentos):
                print("Número insuficiente de argumentos para filter.")
                sys.exit(1)

            r = float(argumentos[i + 1])
            g = float(argumentos[i + 2])
            b = float(argumentos[i + 3])
            imagen_en_pixels = filter(imagen_en_pixels, r, g, b)
            i += 3  # AVANZA 3 POS PARA EIVTAR DUPLICAR ARG

        else:
            print("Transformación no válida: ",funcion)
            sys.exit(1)

        i += 1  # AVANZA AL SGTE ARG

    # REVISA SI LA IMAGEN TIENE EXTENSION, ESTO ES PARA PASAR LOS TEST
    partes_ruta = imagen_entrada.split("/")

    # HACE LO QUE PIDE EL TEST
    if len(partes_ruta)>1:

        extension=partes_ruta[1].split(".")
        nombre_sin_extension = partes_ruta[0]+"/"+extension[0]

    else:
        sin_extension = imagen_entrada.split(".")
        nombre_sin_extension = sin_extension[0]

    # NOMBRE DEL ARCHIVO DE SALIDA
    nombre_salida = nombre_sin_extension + "_trans.jpg"

    write_img(imagen_en_pixels, nombre_salida)

if __name__ == "__main__":
    main()
