import sys
from transforms import *
from images import read_img, write_img

def main():
    # LEE ARGUMENTOS
    argumentos = sys.argv[1:]

    # NOMBRE DE IMAGEN
    imagen_entrada = argumentos[0]

    imagen_en_pixels = read_img(imagen_entrada)

    # REVISA Y HACE TODAS LAS FUNCIONES
    for funcion in argumentos[1:]:

        if funcion == "mirror":
            imagen_en_pixels = mirror(imagen_en_pixels)

        elif funcion == "grayscale":
            imagen_en_pixels = grayscale(imagen_en_pixels)

        elif funcion == "rotate_right":
            imagen_en_pixels = rotate_right(imagen_en_pixels)

        elif funcion == "blur":
            imagen_en_pixels = blur(imagen_en_pixels)

        else:
            print("Función no válida: ", funcion, ". Las opciones son: mirror, grayscale, rotate_right, blur")
            sys.exit(1)

    # REVISA SI LA IMAGEN TIENE EXTENSION, ESTO ES PARA PASAR LOS TEST
    partes_ruta = imagen_entrada.split("/")

    # HACE LO QUE PIDE EL TEST
    if len(partes_ruta)>1:

        extension=partes_ruta[1].split(".")
        nombre_sin_extension = partes_ruta[0]+"/"+extension[0]

    else:
        sin_extension = imagen_entrada.split(".")
        nombre_sin_extension = sin_extension[0]

    # NOMBRE DEL ARCHIVO DE SALIDA
    nombre_salida = nombre_sin_extension+"_trans.jpg"

    write_img(imagen_en_pixels, nombre_salida)

if __name__ == "__main__":
    main()


