# ENTREGA CONVOCATORIA ENERO
-Nombre completo: 
Pablo Gutierrez Tenorio

-Correo universidad: 
p.gutierrezt.2022@alumnos.urjc.es

-Enlace al video de demostración del proyecto:
https://youtu.be/kxQ47hRWQ7Y

-Apartado "Requisitos mínimos":

Programa transforms.py:

Método change_colors,
método rotate_right,
método mirror,
método rotate_colors,
método blur,
método shift,
método crop,
método grayscale,
método filter.

Programa transform_simple.py

Programa trasnsform_args.py

Programa transform_multi.py

-Apartado "Requisitos opcionales":

Filtros avanzados(tono sepia, luminosidad de la imagen, contraste de la imagen)

