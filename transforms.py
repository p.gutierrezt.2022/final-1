import sys
from images import  *

def copiar_imagen(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    filas=len(image)
    columnas=len(image[0])

    imagen_en_blanco=create_blank(filas,columnas)

    for x in range(filas):
        for y in range(columnas):
            imagen_en_blanco[x][y]=image[x][y]

    return imagen_en_blanco

def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple, to_change_to: tuple) -> list[list[tuple[int, int, int]]]:

    copy_image = copiar_imagen(image)

    cambio_colores=[]

    for fila in copy_image:
        new_fila = []
        for pixel in fila:
            # VERIFICA SI EL PIXEL COINCIDE CON EL COLOR A CAMBIAR Y CAMBIARLO AL NUEVO COLOR
            if pixel == to_change:
                new_fila.append(to_change_to)
            else:
                new_fila.append(pixel)
        cambio_colores.append(new_fila)

    return cambio_colores

def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    copy_image= copiar_imagen(image)

    num_filas=len(copy_image)
    num_columnas=len(copy_image[0])

    imagen_rotada=[]

    for a in range(num_columnas-1,-1,-1): # Va desde la última columna hasta la primera
        filas_invertidas = []
        for fila in copy_image:
            filas_invertidas.append(fila[a])
        imagen_rotada.append(filas_invertidas)

    return imagen_rotada

def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    copy_image = copiar_imagen(image)

    num_filas = len(copy_image)
    num_columnas = len(copy_image[0])

    imagen_mirror=[]

    for inversa in range(num_filas-1,-1,-1): # Recorre las filas en orden inverso
        imagen_mirror.append(copy_image[inversa]) # Agrega a la imagen reflejada

    return imagen_mirror

def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:

    copy_image = copiar_imagen(image)

    num_filas = len(copy_image)
    num_columnas = len(copy_image[0])

    for x in range(num_filas):
        for y in range(num_columnas):
            "COLOR ROJO"
            r=(copy_image[x][y][0]+increment)
            if r >= 256:
                r = r % 256
            if r <= -1:
                r = r + 256
            "COLOR VERDE"
            g=(copy_image[x][y][1] + increment)
            if g >= 256:
                g = g % 256
            if g <= -1:
                g = g + 256
            "COLOR AZUL"
            b=(copy_image[x][y][2] + increment)
            if b >= 256:
                b = b % 256
            if b <= -1:
                b = b + 256

            copy_image[x][y]=r,g,b

    return copy_image

def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    num_filas = len(image)
    num_columnas = len(image[0])

    imagen_desenfocada=[]

    for x in range (num_filas):
        filas = []

        for y in range (num_columnas):

            total_r=0
            total_g=0
            total_b =0
            cantidad_pixeles_vecinos=0

            pixeles_vecinos=[
                (x-1,y), # arriba
                (x+1,y), # abajo
                (x,y-1), # izquierda
                (x,y+1), # derecha
            ]

            for vec_x,vec_y in pixeles_vecinos:
                if 0 <= vec_x < num_filas  and 0 <= vec_y < num_columnas:
                    r,g,b=image[vec_x][vec_y]
                    total_r+=r
                    total_g+=g
                    total_b+=b
                    cantidad_pixeles_vecinos+=1

            promedio_r = total_r//cantidad_pixeles_vecinos
            promedio_g = total_g // cantidad_pixeles_vecinos
            promedio_b = total_b // cantidad_pixeles_vecinos

            filas.append((promedio_r,promedio_g,promedio_b))
        imagen_desenfocada.append(filas)

    return imagen_desenfocada

def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:

    num_filas = len(image)
    num_columnas = len(image[0])

    if horizontal == 0 and vertical == 0:
        return image

    # MOVIMIENTO HORIZONTAL

    if horizontal < 0:
        move_horiz = -horizontal
    else:
        move_horiz = num_filas - horizontal

    mov_horizontal_hecho = []

    for fila1 in range(num_filas):
        posicion_h = fila1 + move_horiz

        if posicion_h < num_filas:
            reajuste_horz = 0
        else:
            reajuste_horz = num_filas

        posicion_final_1 = posicion_h - reajuste_horz
        try:
            desplaz_fila_1 = image[posicion_final_1]
        except IndexError:
            print("LIMITES SUPERADOS")
            sys.exit(1)
        mov_horizontal_hecho.append(desplaz_fila_1)

    # MOVIMIENTO VERTICAL

    if vertical > 0:
        move_vert = vertical
    else:
        move_vert = num_columnas + vertical

    mov_vertical_hecho = []

    for fila2 in mov_horizontal_hecho:
        nueva_fila_movida = []

        for x in range(num_columnas):
            posicion_v = x + move_vert

            if posicion_v < num_columnas:
                reajuste_vert = 0
            else:
                reajuste_vert = num_columnas

            posicion_final_2 = posicion_v - reajuste_vert
            desplaz_fila_2 = fila2[posicion_final_2]
            nueva_fila_movida.append(desplaz_fila_2)

        mov_vertical_hecho.append(nueva_fila_movida)

    return mov_vertical_hecho

def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:

    copy_image = copiar_imagen(image)

    imagen_recortada=[]

    for nx in range(x,x+width): # ANCHO
        nueva_fila=[]

        for ny in range(y,y+height): # ALTURA
            nueva_fila.append(copy_image[nx][ny])

        imagen_recortada.append(nueva_fila)

    return imagen_recortada

def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    copy_image = copiar_imagen(image)

    num_filas = len(copy_image)
    num_columnas = len(copy_image[0])

    for x in range(num_filas):
        for y in range(num_columnas):

            media=sum(copy_image[x][y])//len(copy_image[x][y])

            copy_image[x][y]=(media,media,media)

    return copy_image

def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:

    copy_image = copiar_imagen(image)

    num_filas=len(copy_image)
    num_columnas=len(copy_image[0])

    for x in range(num_filas):
        for y in range(num_columnas):
            "COLOR ROJO"
            rojo=  min( int(copy_image[x][y][0]*r) , 255)
            "COLOR VERDE"
            verde= min( int(copy_image[x][y][1]*g) , 255)
            "COLOR AZUL"
            azul=  min( int(copy_image[x][y][2]*b) , 255)

            copy_image[x][y]=rojo,verde,azul

    return copy_image


# REQUISITOS OPCIONALES: FILTROS AVANZADOS #

def tono_sepia(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:

    copy_image = copiar_imagen(image)

    imagen_sepia = []

    for fila in copy_image:
        nueva_fila= []

        for pixel in fila:
            # ES NECESARIO PRIMERO OBTENER UNA ESCALA DE GRISES PARA NO IR CAMBIANDO CADA COLOR R,G,B POR SEPARADO
            gris = sum(pixel) // 3
            # ESTO SE ENCARGA DE DARLE EL TONO SEPIA QUE BUSCAMOS
            sepia_pixel = (gris + 40, gris + 20, gris)
            nueva_fila.append(sepia_pixel)

        imagen_sepia.append(nueva_fila)

    return imagen_sepia

def modificar_brillo(image: list[list[tuple[int, int, int]]], factor: float) -> list[list[tuple[int, int, int]]]:

    copy_image = copiar_imagen(image)

    # SI ES MENOR QUE 1 SE VERE CON MENOS BRILLO Y SI ES MAYOR QUE 1 AUMENTARA EL BRILLO
    factor = max(0, factor)

    imagen_brillo = []

    for file in copy_image:
        nueva_fila = []

        for pixel in file:
            rojo=min(int(pixel[0]*factor),255)
            verde=min(int(pixel[1]*factor),255)
            azul=min(int(pixel[2]*factor),255)
            nueva_fila.append((rojo,verde,azul))

        imagen_brillo.append(nueva_fila)

    return imagen_brillo

def modificar_contraste(image: list[list[tuple[int, int, int]]], factor: float) -> list[list[tuple[int, int, int]]]:

    copy_image = copiar_imagen(image)

    imagen_contraste=[]

    for fila in copy_image:
        nueva_fila = []

        for pixel in fila:
            # SE RESTA 128 PARA CENTRAR VALORES A 0, SE MULTIPLICA PARA AJUSTAR LA INTENSIDAD, YSE SUMA 128 PARA CENTRAR VALORES AL RANGO ORIGINAL
            rojo  =  int((pixel[0] - 128) * factor + 128)
            verde =  int((pixel[1] - 128) * factor + 128)
            azul  =  int((pixel[2] - 128) * factor + 128)
            nueva_fila.append((rojo,verde,azul))

        imagen_contraste.append(nueva_fila)

    return imagen_contraste