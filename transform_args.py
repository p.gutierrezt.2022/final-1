import sys
from transforms import *
from images import read_img, write_img

def main():
    # LEE ARGUMENTOS

    argumentos=sys.argv[1:]

    if len(argumentos) < 2:
        print("Falta argumentos.")
        sys.exit(1)

    else:
        imagen_entrada = argumentos[0]
        transformacion = argumentos[1]
        parametros = sys.argv[3:]

    if transformacion == "shift":

        imagen_en_pixels = read_img(imagen_entrada)

        horizontal = int(parametros[0])
        vertical = int(parametros[1])

        imagen_trasformada = shift(imagen_en_pixels, horizontal, vertical)

    elif transformacion == "change_colors":

        if len(parametros) != 6:
            print("La función change_colors requiere de 6 parametros para los valores RGB.")
            sys.exit(1)

        imagen_en_pixels = read_img(imagen_entrada)

        prim_parametros=parametros[:3]
        to_change = []
        for t in prim_parametros:
            entero_conv = int(t)
            to_change.append(entero_conv)
        to_change = tuple(to_change)

        sec_parametros=parametros[3:]
        to_change_to = []
        for tt in sec_parametros:
            entero_conv = int(tt)
            to_change_to.append(entero_conv)
        to_change_to = tuple(to_change_to)

        imagen_trasformada = change_colors(imagen_en_pixels, to_change, to_change_to)

    elif transformacion == "rotate_colors":

        if len(parametros) != 1:
            print("La función rotate_colors requiere solo un número entero como parámetro.")
            sys.exit(1)

        imagen_en_pixels = read_img(imagen_entrada)

        incremento = int(parametros[0])

        imagen_trasformada = rotate_colors(imagen_en_pixels, incremento)

    elif transformacion == "crop":

        if len(parametros) != 4:
            print("La función crop requiere de 4 parametros.")
            sys.exit(1)

        imagen_en_pixels = read_img(imagen_entrada)

        x = int(parametros[0])  # coordenada x de la esquina superior izquierda del rectángulo
        y = int(parametros[1])  # coordenada y de la esquina superior izquierda del rectángulo
        width = int(parametros[2])  # ancho
        height = int(parametros[3])  # altura

        imagen_trasformada = crop(imagen_en_pixels, x, y, width, height)

    elif transformacion == "filter":

        if len(parametros) != 3:
            print("La función filter requiere de 3 parametros.")
            sys.exit(1)

        imagen_en_pixels = read_img(imagen_entrada)

        r = float(parametros[0])
        g = float(parametros[1])
        b= float(parametros[2])

        imagen_trasformada = filter(imagen_en_pixels, r, g, b)

    elif transformacion == "rotate_right":

        imagen_en_pixels = read_img(imagen_entrada)

        imagen_trasformada = rotate_right(imagen_en_pixels)

    elif transformacion == "mirror":

        imagen_en_pixels = read_img(imagen_entrada)

        imagen_trasformada = mirror(imagen_en_pixels)

    elif transformacion == "grayscale":

        imagen_en_pixels = read_img(imagen_entrada)

        imagen_trasformada = grayscale(imagen_en_pixels)

    elif transformacion == "blur":

        imagen_en_pixels = read_img(imagen_entrada)

        imagen_trasformada = blur(imagen_en_pixels)


    else:
        print("Función no válida: ", imagen_entrada, ". Las opciones son: mirror, shift, change_colors, rotate_colors, crop, filter, rotate_right, mirror, grayscale, blur.")
        sys.exit(1)



    # REVISA SI LA IMAGEN TIENE EXTENSION, ESTO ES PARA PASAR LOS TEST
    partes_ruta = imagen_entrada.split("/")

    # HACE LO QUE PIDE EL TEST
    if len(partes_ruta)>1:

        extension=partes_ruta[1].split(".")
        nombre_sin_extension = partes_ruta[0]+"/"+extension[0]

    else:
        sin_extension = imagen_entrada.split(".")
        nombre_sin_extension = sin_extension[0]

    # NOMBRE DEL ARCHIVO DE SALIDA
    nombre_salida = nombre_sin_extension + "_trans.jpg"

    write_img(imagen_trasformada, nombre_salida)


if __name__ == "__main__":
    main()